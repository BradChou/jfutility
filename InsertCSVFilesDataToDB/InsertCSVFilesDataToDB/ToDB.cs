﻿using Microsoft.Extensions.Configuration;
using ServiceReference1;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace InsertCSVFilesDataToDB
{
    public class ToDB
    {

        static IConfiguration config = new ConfigurationBuilder()
      .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
      .Build();


        static readonly string LogPath = config["LogPath"];


        public async Task CBMToTableDeliveryScanLog(List<DeliveryScanLog> Entities, string file)
        {
            //   string str = ".csv";
            string str2 = file;
            string logFileName = LogPath + str2 + ".txt";
            List<string> error = new List<string>();
            try
            {
                 WebServiceSoapClient webServiceSoapClient = new WebServiceSoapClient(WebServiceSoapClient.EndpointConfiguration.WebServiceSoap);

                for (int i = 0; i < Entities.Count; i++)
                {
                    string x = await webServiceSoapClient.InsertDeliveryScanLogFromCBMAsync(Entities[i].ScanDate, Entities[i].DriverCode, Entities[i].CheckNumber);
                    //string x = "x";
                    Console.WriteLine("{2}。共{0}筆CSV資料，第{1}筆已經寫入資料庫", Entities.Count, i + 1, x);
                    error.Add(string.Concat(new string[]
                    {
                        DateTime.Now.ToString(),
                        x,
                        "。共",
                        Entities.Count.ToString(),
                        "筆CSV資料，第",
                        (i + 1).ToString(),
                        "筆貨號_",
                        Entities[i].CheckNumber,
                        "_已經寫入資料庫"
                    }));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("CBMToTableDeliveryScanLog Function " + ex.Message);
                error.Add(DateTime.Now.ToString() + "_CBMToTableDeliveryScanLog Function_" + ex.Message);
                File.AppendAllLines(logFileName, error);
            }
            error.Add("----------------------------------------");
            File.AppendAllLines(logFileName, error);
        }
    }
}
