﻿using ServiceReference1;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration.Attributes;
using System.Globalization;
using CsvHelper.Configuration;
using Microsoft.Extensions.Configuration;
using System.Linq;

namespace InsertCSVFilesDataToDB
{
    public class Program
    {

        static IConfiguration config = new ConfigurationBuilder()
       .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
       .Build();


        static readonly string TaichungPath = config["TaichungPath"];
        static readonly string DaYuanPath = config["DaYuanPath"];
        static readonly string LogPath = config["LogPath"];
        static readonly string DayAgo = config["DayAgo"];//幾天前的資料比對


        static async Task Main(string[] args)
        {
            Console.WriteLine("開始把台中倉和大園倉的csv掃描時間寫入DeliveryScanlog發送時間");



            try
            {
                int Days = string.IsNullOrEmpty(DayAgo) ? 5 : int.Parse(DayAgo);

                //TaichungPath CSV
                var TaichungdirList = new DirectoryInfo(TaichungPath);
                var TaichungFiles = TaichungdirList.GetFiles("*.csv");
                var TaichungFilesName = TaichungFiles.Where(x => x.LastWriteTime > DateTime.Now.AddDays(-1 * Days)).Select(x => x.Name).ToList();
                //DaYuan CSV
                var DaYuandirList = new DirectoryInfo(DaYuanPath);
                var DaYuanFiles = DaYuandirList.GetFiles("*.csv");
                var DaYuanFilesName = DaYuanFiles.Where(x => x.LastWriteTime > DateTime.Now.AddDays(-1 * Days)).Select(x => x.Name).ToList();


                //LOG TXT
                var dirListTXT = new DirectoryInfo(LogPath);
                var FilesTXT = dirListTXT.GetFiles("*.txt");
                var FilesTXTName = FilesTXT.Where(x => x.LastWriteTime > DateTime.Now.AddDays(-1 * Days)).Select(x => x.Name.Replace(".txt", "")).ToList();


                //當下的小時不需要 怕檔案還沒有完成
                FilesTXTName.Add(DateTime.Now.ToString("yyyyMMddHH"));


                //抓取位抓取的檔案
                var NewTaichungList = TaichungFilesName.Except(FilesTXTName).ToList();
                var NewDaYuanList = DaYuanFilesName.Except(FilesTXTName).ToList();




                ToDB toDB = new ToDB();

                foreach (var file in NewTaichungList)
                {
                    var TaiChungData = GetTaichungCSVData(file);
                    if (TaiChungData.Count > 0)
                    {
                        await toDB.CBMToTableDeliveryScanLog(TaiChungData, file);
                    }
                }


                foreach (var file in NewDaYuanList)
                {
                    var DaYuanData = GetDaYuanCSVData(file);
                    if (DaYuanData.Count > 0)
                    {
                        await toDB.CBMToTableDeliveryScanLog(DaYuanData, file);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("作業結束");

        }

        static List<DeliveryScanLog> GetTaichungCSVData(string file)
        {
            string fileName = file;
            string path = LogPath + fileName + ".txt";
            string fullPath = System.IO.Path.Combine(TaichungPath, fileName);
            List<string> list2 = new List<string>();
            List<DeliveryScanLog> deliveryScanLogs = new List<DeliveryScanLog>();
            int j = 0;
            try
            {
                string[] files = System.IO.Directory.GetFiles(TaichungPath, fileName);

                if (files.Length > 0)
                {
                    using (var reader = new StreamReader(File.OpenRead(fullPath)))
                    using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                    {
                        csv.Configuration.HasHeaderRecord = false;
                        csv.Configuration.RegisterClassMap<CSVDataMap>();
                        var records = csv.GetRecords<CSVData>();

                        foreach (var data in records)
                        {
                            try
                            {
                                if (data.Status == "OK")
                                {
                                    j++;
                                    DeliveryScanLog log = new DeliveryScanLog();
                                    log.CheckNumber = data.CheckNumber;
                                    log.ScanDate = data.ScanDate;
                                    log.DriverCode = "PP491";// "C1017";  // 台中倉丈量機
                                    deliveryScanLogs.Add(log);
                                    list2.Add(string.Concat(new string[] { DateTime.Now.ToString(), "_GetTaichungCSVData Function_", fileName, "_第", (j).ToString(), "筆_", data.CheckNumber.Trim(), "_成功讀取" }));
                                }
                            }
                            catch (Exception ex)
                            {
                                list2.Add(string.Concat(new string[] { DateTime.Now.ToString(), "_GetTaichungCSVData Function_", fileName, "_第", (j).ToString(), "筆_", data.CheckNumber.Trim(), "_", ex.Message }));
                                File.AppendAllLines(path, list2);

                            }

                        }
                    }
                    Console.WriteLine("台中倉CSV資料讀取完畢" + fileName);
                    list2.Add(string.Concat(new string[] { DateTime.Now.ToString(), "_GetTaichungCSVData Function_台中倉_", fileName, "_實際共", j.ToString(), "筆資料讀取完畢" }));
                    list2.Add("----------------------------------------");
                }
                else
                {
                    Console.WriteLine("台中倉並無上一小時的CSV檔案" + fileName);
                    list2.Add(DateTime.Now.ToString() + "台中倉並無上一小時的CSV檔案" + fileName);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("InsertTaichungData Function " + e.Message);
                list2.Add(string.Concat(new string[]
                {
                   "InsertDaYuanData Function " + e.Message
                }));
                File.AppendAllLines(path, list2);
            }
            File.AppendAllLines(path, list2);
            return deliveryScanLogs;
        }


        static List<DeliveryScanLog> GetDaYuanCSVData(string file)
        {
            //string DaYuanPath = @"\\52.15.86.18\storage-for-station\CSVFromDaYuan";


            string fileName = file;
            //string fileName = "2020112603.csv"; 
            string fullPath = System.IO.Path.Combine(DaYuanPath, fileName);
            string path = LogPath + fileName + ".txt";
            List<string> list2 = new List<string>();
            List<DeliveryScanLog> deliveryScanLogs = new List<DeliveryScanLog>();
            int j = 0;
            try
            {
                string[] files = System.IO.Directory.GetFiles(DaYuanPath, fileName);

                if (files.Length > 0)
                {
                    using (var reader = new StreamReader(File.OpenRead(fullPath)))
                    using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                    {
                        csv.Configuration.HasHeaderRecord = false;
                        csv.Configuration.RegisterClassMap<CSVDataMap>();
                        var records = csv.GetRecords<CSVData>();

                        foreach (var data in records)
                        {
                            try
                            {

                                if (data.Status == "OK")
                                {
                                    DeliveryScanLog log = new DeliveryScanLog();
                                    log.CheckNumber = data.CheckNumber;
                                    log.ScanDate = data.ScanDate;
                                    log.DriverCode = "PP002"; //大園倉丈量機
                                    deliveryScanLogs.Add(log);
                                    list2.Add(string.Concat(new string[] { DateTime.Now.ToString(), "_GetDaYuanCSVData Function_", fileName, "_第", (j).ToString(), "筆_", data.CheckNumber.Trim(), "_成功讀取" }));
                                }
                            }
                            catch (Exception ex)
                            {
                                list2.Add(string.Concat(new string[] { DateTime.Now.ToString(), "_GetDaYuanCSVData Function_", fileName, "_第", (j).ToString(), "筆_", data.CheckNumber.Trim(), "_", ex.Message }));
                                File.AppendAllLines(path, list2);

                            }
                        }
                    }
                    Console.WriteLine("大園倉CSV資料讀取完畢" + fileName);
                    list2.Add(string.Concat(new string[] { DateTime.Now.ToString(), "_GetDaYuanCSVData Function_大園倉_", fileName, "_實際共", j.ToString(), "筆資料讀取完畢" }));
                    list2.Add("----------------------------------------");
                }
                else
                {
                    Console.WriteLine("大園倉並無上一小時的CSV檔案" + fileName);
                    list2.Add(DateTime.Now.ToString() + "大園倉並無上一小時的CSV檔案" + fileName);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("InsertDaYuanData Function " + e.Message);
                list2.Add(string.Concat(new string[]
                {
                   "InsertDaYuanData Function " + e.Message
                }));
                File.AppendAllLines(path, list2);
            }
            File.AppendAllLines(path, list2);
            return deliveryScanLogs;
        }

    }



    public class DeliveryScanLog
    {
        public string CheckNumber { get; set; }

        public DateTime ScanDate { get; set; }

        public string DriverCode { get; set; }

    }

    public class CSVData
    {
        [Name("託運單號"), Index(0)]
        public string CheckNumber { get; set; }

        [Name("掃描日期"), Index(6)]
        public DateTime ScanDate { get; set; }

        [Name("測量狀態"), Index(7)]
        public string Status { get; set; }
    }

    public class CSVDataMap : ClassMap<CSVData>
    {
        public CSVDataMap()
        {
            Map(m => m.CheckNumber).Index(0);
            Map(m => m.ScanDate).Index(6);
            Map(m => m.Status).Index(7);
        }
    }
}


