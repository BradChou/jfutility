﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace UploadFilesToS3
{
    class Program
    {
        //string sourcePath = @"C:\DaYuanUploadCSVFilesToS3logs\Log";
        //  string sourcePath = @"C:\TaichungUploadCSVFilesToS3logs\Log";
        //string targetPath = @"\\52.15.86.18\storage-for-station\CSVFromDaYuan";
        //   string targetPath = @"\\52.15.86.18\storage-for-station\CSVFromTaichung";
        static string sourcePath = @"C:\DaYuanUploadCSVFilesToS3logs\";
        static string targetPath = @"\\52.15.86.18\storage-for-station\CSVFromDaYuan";
        static string logPath = @"C:\條碼材積讀取程式\Log";

        static void Main(string[] args)
        {
            int c = 1;//前幾小時

            for (int i = 1; i <= c; i++)
            {

                List<string> list = new List<string>();
                string fileType = ".csv";
                string fileName = DateTime.Now.AddHours(-1 * i).ToString("yyyyMMddHH") + fileType;
                string path = sourcePath + fileName + ".txt";
                try
                {
                    string sourceFullFilePath = string.Format("{0}\\{1}", logPath, fileName);
                    if (System.IO.Directory.GetFiles(logPath, fileName).Length != 0)
                    {
                        string destFile = System.IO.Path.Combine(targetPath, fileName);
                        System.IO.File.Copy(sourceFullFilePath, destFile, true);
                        Console.WriteLine("複製 {0} 成功", sourceFullFilePath);
                        list.Add(DateTime.Now.ToString() + "_複製" + sourceFullFilePath + "成功");
                        File.AppendAllLines(path, list);
                    }
                    else
                    {
                        Console.WriteLine("上一小時並無csv檔案" + fileName);
                        list.Add(DateTime.Now.ToString() + logPath + "上一小時並無csv檔案");
                        File.AppendAllLines(path, list);
                    }
                }
                catch (Exception ex)
                {
                    list.Add(DateTime.Now.ToString() + ex.Message);
                    File.AppendAllLines(path, list);
                }
            }

        }
    }
}
