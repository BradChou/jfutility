﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;

namespace InsertPhotoPathToDB
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("開始把台中倉和大園倉的圖片路徑寫入DeliveryRequests");

            try
            {
                var TaiChungPicPathData = GetPicPath(49);
                if (TaiChungPicPathData.Count > 0)
                    ToDB.PicturesPathToTableDeliveryRequests(TaiChungPicPathData);

                var DaYuanPicPathData = GetPicPath(29);
                if (DaYuanPicPathData.Count > 0)
                    ToDB.PicturesPathToTableDeliveryRequests(DaYuanPicPathData);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("作業結束");
        }

        static List<DeliveryRequests> GetPicPath(int station) //大園倉 = 29 , 台中倉 = 49
        {
            string TaichungPath = @"\\52.15.86.18\storage-for-station\TaiChungCbm\pictures\";
            string TaichungFileName = DateTime.Now.ToString("yyyyMMdd") + @"\";

            string DaYuanPath = @"\\52.15.86.18\storage-for-station\DaYuanCbm\pictures\";
            string DaYuanFileName = DateTime.Now.ToString("yyyyMMdd") + @"\";
            //string DaYuanFileName = @"20201132\";

            string fileType = "*08.jpg";
            List<DeliveryRequests> fileNameList = new List<DeliveryRequests>();

            try
            {
                if (station == 49)
                {
                    DirectoryInfo dir = new DirectoryInfo(TaichungPath + TaichungFileName);

                    if (dir.Exists)
                    {
                        FileInfo[] filesArray = dir.GetFiles(fileType);

                        if (filesArray.Length > 0)
                        {
                            foreach (var file in filesArray)
                            {
                                DeliveryRequests deliveryRequests = new DeliveryRequests();

                                deliveryRequests.CheckNumber = file.Name.Substring(0, file.Name.IndexOf("-"));
                                deliveryRequests.PicPathCBM = file.FullName.Replace(@"\\52.15.86.18\storage-for-station\", "");
                                deliveryRequests.Station = 49;

                                fileNameList.Add(deliveryRequests);
                            }
                            Console.WriteLine("台中倉圖片檔名已讀取完畢");
                        }
                        else
                        {
                            Console.WriteLine("台中倉資料夾內找不到符合的file type");
                        }
                    }
                    else
                    {
                        Console.WriteLine("台中倉並無當日的圖片資料夾");
                    }
                }
                else if (station == 29)
                {
                    DirectoryInfo dir = new DirectoryInfo(DaYuanPath + DaYuanFileName);

                    if (dir.Exists)
                    {
                        FileInfo[] filesArray = dir.GetFiles(fileType);

                        if (filesArray.Length > 0)
                        {
                            foreach (var file in filesArray)
                            {
                                DeliveryRequests deliveryRequests = new DeliveryRequests();

                                deliveryRequests.CheckNumber = file.Name.Substring(0, file.Name.IndexOf("-"));
                                deliveryRequests.PicPathCBM = file.FullName.Replace(@"\\52.15.86.18\storage-for-station\", "");
                                deliveryRequests.Station = 29;

                                fileNameList.Add(deliveryRequests);
                            }
                            Console.WriteLine("大園倉圖片檔名已讀取完畢");
                        }
                        else
                        {
                            Console.WriteLine("大園倉資料夾內找不到符合的file type");
                        }
                    }
                    else
                    {
                        Console.WriteLine("大園倉並無當日的圖片資料夾");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("GetPicPath Function " + e.Message);
            }
            return fileNameList;
        }
    }

    public class ToDB
    {
        public static void PicturesPathToTableDeliveryRequests(List<DeliveryRequests> Entities)
        {
            try
            {
                string column = "";
                string showtext = "";

                if (Entities.Select(a => a.Station).FirstOrDefault() == 29)
                {
                    column = "catch_cbm_pic_path_from_S3";
                    showtext = "大園倉";
                }
                if (Entities.Select(a => a.Station).FirstOrDefault() == 49)
                {
                    column = "catch_taichung_cbm_pic_path_from_S3";
                    showtext = "台中倉";
                }

                using (SqlCommand cmd = new SqlCommand())
                using (cmd.Connection = new SqlConnection(@"Initial Catalog=JunfuReal;server=61.222.94.197,342; Initial Catalog = JunFuReal; Persist Security Info = True; User Id=lyAdmin;Password=P@ssw0rd!!!"))
                {
                    cmd.Connection.Open();

                    for (int i = 0; i < Entities.Count; i++)
                    {
                        cmd.CommandText = string.Format(@"update tcDeliveryRequests set {0} = '{1}' where check_number = '{2}'", column, Entities[i].PicPathCBM, Entities[i].CheckNumber);
                        cmd.ExecuteNonQuery();
                        Console.WriteLine("{2}共{0}個圖片檔名，第{1}筆已經寫入資料庫", Entities.Count, i + 1, showtext);
                    }
                    cmd.Connection.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("PicturesPathToTableDeliveryRequests Function " + e.Message);
            }
        }
    }

    public class DeliveryRequests
    {
        public string CheckNumber { get; set; }

        public string PicPathCBM { get; set; }

        public int Station { get; set; }
    }
}
